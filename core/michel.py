#--------------------------------#
#--- Pour les couches limites ---#
#--------------------------------#

import numpy as np

#--- Loi de viscosite RENNES
def FNMU(GAZ,MUREF,TREF,T):
    if GAZ == "He":
        return MUREF*(T/TREF)**0.647
    elif GAZ == "Ar":
        return MUREF*(T/TREF)**0.93
    elif GAZ == "N2":
        return MUREF*(T/TREF)**0.5*(1+114/TREF)/(1+114/T)

#--- Loi de temperature de l'ecoulement
def FNTE(T0,GM1,MACH):
    return T0/(1+GM1/2*MACH**2)

#--- Temperature de frottement dans la couche limite
def FNTF(RT,GM1,MACH,T):
    return T*(1+RT*GM1/2*MACH**2)

#--- Parametre de forme
def FNH(HI,ALPHA,BETA,TP,MACH,T1,T2):
    return HI+ALPHA*MACH**2+BETA*(TP-T2)/T1

#--- Loi de temperature de Monaghan
def FNTS(TP,T1,T2):
    return T1+0.54*(TP-T1)+0.16*(T2-T1)

#--- Loi de densite de l'ecoulement
def FNRHO(GM1I,RHO0,T0,T):
    return RHO0*(T/T0)**GM1I

#--- Loi de vitesse de l'ecoulement
def FNU(GAM,BOLTZ,M,MACH,T):
    return MACH*(GAM*BOLTZ*T/M)**0.5

#--- résolution du problème
def michel(Z,R,THETA,MACH,PARAMS,CST):

    if(PARAMS.VERBOSITY):
        print('-----------------------------------')
        print('    Compute the boundary layers    ')
        print('-----------------------------------')

    #--- Calcul des differents coefficients en fonction de gammas
    GP1=CST.GAM+1
    GM1=CST.GAM-1
    GPSM=GP1/GM1
    GMSP=GM1/GP1
    GM1I=1/GM1
    GSM1=CST.GAM/GM1

    # recalibrer Z,R pour R[0] coincide à ce stade avec 1 exactement de manière a obtenir exactement R[0]=RCOL+EDC à la fin
    # note : opération facultative, la recalibration peut s'effectuer à la fin
    NPTS=np.where(Z==0)[0][0]-1
    Z=Z/R[NPTS]
    R=R/R[NPTS]

    #--- Recuperer le profil isentropique
    Z=Z*PARAMS.RCOL
    R=R*PARAMS.RCOL
    THE=THETA
    MAC=MACH

    Z=np.flip(Z[:NPTS+1], axis=0)
    R=np.flip(R[:NPTS+1], axis=0)
    THE=np.flip(THE[:NPTS+1], axis=0)
    MAC=np.flip(MAC[:NPTS+1], axis=0)

    UE=np.zeros([NPTS+1])       # vitesse exterieur
    DMAC=np.zeros([NPTS+1])     # derivee du nombre de Mach du noyau isentropique
    DUE=np.zeros([NPTS+1])      # derivee de la vitesse du noyau isentropique
    F=np.zeros([NPTS+1])        # fonction phi
    E=np.zeros([NPTS+1])        # fonction E
    INE=np.zeros([NPTS+1])      # partie integrale de la fonction E
    IN=np.zeros([NPTS+1])       # partie integrale de la solution de Michel
    DELTA=np.zeros([NPTS+1])    #
    DELTA1=np.zeros([NPTS+1])   # epaisseur de deplacement du profil
    DELTA2=np.zeros([NPTS+1])   # epaisseur de deplacement de la quantite de mouvement
    RAPTEMP=np.zeros([NPTS+1])  # rapport de temperatures
    H=np.zeros([NPTS+1])        # parametre de forme
    MU=np.zeros([NPTS+1])       # viscosite dynamique du gaz
    G=np.zeros([NPTS+1])        # facteur de compressibilite
    RHO=np.zeros([NPTS+1])      # densite du noyau isentropique

    DELTA1[0] = PARAMS.EDC

    #--- Calcul du debit massique et du debit vol.
    RHO0=PARAMS.P0/PARAMS.T0*CST.M*CST.AVO/CST.R_CST_UNIV
    TC=PARAMS.T0*2/GP1
    PC=PARAMS.P0*(2/GP1)**GSM1
    RHOC=PC/TC*CST.M*CST.AVO/CST.R_CST_UNIV
    UC=(CST.GAM*CST.BOLTZ*TC/CST.M)**0.5
    Q=RHOC*UC*np.pi*PARAMS.RCOL**2

    #--- Calcul de quantites necessaires à la solution integrale de Michel
    for I in range(NPTS+1):
        TE=FNTE(PARAMS.T0,GM1,MAC[I])         # temperature de l'ecoulement exterieur
        TF=FNTF(CST.RT,GM1,MAC[I],TE)      # temperature de frottement
        TS=FNTS(PARAMS.TP,TE,TF)          # temperature de Monaghan
        MUS=FNMU(PARAMS.GAZ,CST.MUREF,CST.TREF,TS)            # viscosite à la temperature de Monaghan
        MU[I]=FNMU(PARAMS.GAZ,CST.MUREF,CST.TREF,TE)          # viscosite de l'ecoulement exterieur
        H[I]=FNH(CST.HI,CST.ALPHA,CST.BETA,PARAMS.TP,MAC[I],TE,TF)  # parametre de forme
        G[I]=TE/TS*MUS/MU[I]    # facteur de compressibilite
        RHO[I]=FNRHO(GM1I,RHO0,PARAMS.T0,TE)        # densite de l'ecoulement exterieur
        UE[I]=FNU(CST.GAM,CST.BOLTZ,CST.M,MAC[I],TE)    # vitesse de l'ecoulement exterieur
        F[I]=(UE[I]**(CST.HI+2)*RHO[I]**(1-CST.ALPHA))**(CST.M0+1)  # fonction phi
        # pour la fonction E
        RAPTEMP[I]=(PARAMS.TP-TF)/TE#*TE/T0 # note TE ~ T0
        if I != NPTS:           # derivee du nombre de Mach à l'ordre 1 pour ensuite calculer la derivee de la vitesse de l'ecoulement exterieur
            DMAC[I]=(MAC[I+1]-MAC[I])/(Z[I+1]-Z[I])
        else:
            DMAC[I]=0

    # --- Calcul de la fonction E
    for I in range(1,NPTS+1):   # integration de l'integrale de la fonction E
        INE[I]=INE[I-1]+.5*(Z[I]-Z[I-1])*(RAPTEMP[I-1]*DMAC[I-1]/MAC[I-1]+RAPTEMP[I]*DMAC[I]/MAC[I])
    E=np.exp(CST.BETA*(CST.M0+1)*INE)     # fonction E

    #-- Calcul de l'integrale de la solution de Michel
    for I in range(1,NPTS+1):
        F1=G[I-1]*F[I-1]*E[I-1]*R[I-1]**2*MU[I-1]/RHO[I-1]/UE[I-1]
        F2=G[I]*F[I]*E[I]*R[I]**2*MU[I]/RHO[I]/UE[I]
        IN[I]=IN[I-1]+0.5*(Z[I]-Z[I-1])*(F1+F2)

    B0=F[0]*E[0]*(DELTA1[0]*R[0]/H[0])**(CST.M0+1)
    #-- Et finalement DELTA2, DELTA1, DELTA suivant la formule de Michel
    DELTA2[1:]=((B0+CST.K*(CST.M0+1)*IN[1:])/F[1:]/E[1:])**(1/(CST.M0+1))/R[1:]
    DELTA1[1:]=H[1:]*DELTA2[1:]
    DELTA[1:]=DELTA1[1:]*(1+2.5*CST.HI/H[1:])

    if(PARAMS.VERBOSITY):
        print("Debit = ","{:.3f}".format(Q*1e3)," g/s = ","{:.3f}".format(Q/CST.M/CST.AVO*CST.Vm_CNTP*1e3*60)," l/mn")
        print("Boundary layer thickness at exit = ","{:.3f}".format(DELTA[NPTS]*100), "cm")
        print("Isentropic radius at exit = ","{:.3f}".format(R[NPTS]*100)," cm")
        print("delta/r = ","{:.3f}".format(DELTA[NPTS]/R[NPTS]*100), " %")
        print("Nozzle length = ","{:.3f}".format(Z[NPTS]*100)," cm")

    return Z,R,DELTA,DELTA1
