class CONSTANTES:

    def __init__(self,GAZ):

        self.R_CST_UNIV=8.31446261815324
        self.AVO=6.02214076E+23
        self.BOLTZ=1.38E-23
        self.Vm_CNTP=22.414e-3 # volume molaire dans les conditions normales de temperature (0°C) et de pression (1 atm)

        if GAZ == "He":
            self.GAM=5./3             # Indice adiabatique
            self.M=4.0026            # Masse molaire (g/mol)
            self.PRAN=0.66           # Nombre de Prandlt = mu*cp/cond. Therm.
            self.MUREF=1.887E-05     # Viscosite à la temperature Tref (Pa.s)
            self.TREF=273
            self.CP=5193             # Chaleur specifique en J/kg/K
        elif GAZ == "Ar":
            self.GAM=5./3
            self.M=39.948
            self.PRAN=0.66
            self.MUREF=2.116E-05
            self.TREF=273
            self.CP=519.3
        elif GAZ == "N2":
            self.GAM=7./5
            self.M=28.013
            self.PRAN=.73
            self.MUREF=5.33e-6       # valeur Marquette 5.48E-06
            self.TREF=80.6           # valeur Marquette 78.6
            self.CP=1033

        #-- Homogeneisation en SI des unites
        self.M=self.M*1e-3/self.AVO     # masse atomique du gaz vecteur : g/mol -> kg

        #--- Expression de Michel pour les couches limites
        self.RT=self.PRAN**0.5 # Facteur thermique parietal laminaire
        self.ALPHA=self.GAM*self.BOLTZ/self.CP/self.M*2.05336*self.PRAN**0.63751 # Coefficients du parametre de forme et geometriques
        self.BETA=-0.71107*self.PRAN+3.41551
        self.HI=2.591 # parametre de forme en incompressible
        self.M0=1
        self.K=.2205 # constante "b" de la solution de Michel
