class PARAMETRES:

    NOMTUYERE='M4_200mbar_N2_test2.dat' # Fichier de sauvergarde du profil de tuyere

    ME  = 4         # Nombre de Mach
    GAZ = 'He'      # "He", "Ar", "N2" # Nature du gaz
    EPS = 1e-002    # Pas de calcul
    PTB = 16        # Parametre de longueur (entier)
    T   = 50        # Temperature d'écoulement

    RCOL = 1.       # Rayon au col (cm)
    P0   = 200.     # Pression de reservoir (mbar)
    T0   = 293.     # Temperature de reservoir
    TP   = 293.     # Temperature de paroi
    EDC  = 1e-004   # Epaisseur deplacement col(m)

    RCT=5           # Rayon de courbure au col
    NPC=9001        # Nombre de points pour le convergent
    NIIC=100
    CS=EPS
    BME=PTB-EPS
    CVGL=1e-006     # critere de convergence
    CVGC=CVGL
    
    VERBOSITY=False
    BOUNDARYLAYER=True
    PLOT=True


    #-- Homogeneisation en SI des unites
    P0=P0*100        # pression reservoir : mbar -> Pa
    RCOL=RCOL/100    # rayon de col : cm -> m
