#                       Programme  ESU                               
#           Auteur :    O. Durif - olivier@durif.fr     

This program computes the isentropic kernel of a supersonic jet          
by the method of characteristics following the Owen's work.          
Then computes the laminar boundary layer and the profile          
of a supersonic nozzle from an isentropic kernel and          
generating conditions given by Michel's integral method.

---------------------------

Ce programme calcule le noyau isentropique d'un jet supersonique          
par la methode des caracteristiques suivant les travaux d'Owen.                        
Puis calcule la couche limite laminaire et le profil                     
d'une tuyere supersonique a partir d'un noyau isentropique et           
de conditions generatrices donnees par la methode integrale de Michel.

# Run on Heroku

https://lavalnozzle.herokuapp.com/
